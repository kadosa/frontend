import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Product } from './product';
import { Observable } from 'rxjs/Observable';
import { ReplaySubject } from 'rxjs/ReplaySubject';

@Injectable()
export class ProductsService {
  private productsUrl = 'api/products';
  private _products: Observable<Product[]>;
  private dataObs$ = new ReplaySubject(1);
  private dataObject;

  constructor(private http: Http) { }

  // return the cached version of the observable on subsequent calls
  // @todo make this work
  getProducts(forceRefresh?: boolean): Observable<Product[]> {
    if (!this.dataObs$.observers.length || forceRefresh) {
      this.http.get(this.productsUrl)
                        .map(this.extractData)
                        .catch(this.handleError)
                        .subscribe(
                          data => this.dataObs$.next(data),
                          error => {
                              this.dataObs$.error(error);
                              // Recreate the Observable as after Error we cannot emit data anymore
                              this.dataObs$ = new ReplaySubject(1);
                            }
                        );
    }
    return this.dataObs$;
  }

  private extractData(res: Response) {
    let data = res.json();
    return data || { };
  }

  private handleError(error: Response | any) {
    // In a real world app, we might use a remote logging infrastructure
    let errMsg: string;
    if (error instanceof Response) {
      const body = error.json() || '';
      const err = body.error || JSON.stringify(body);
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    return Observable.throw(errMsg);
  }

}
