import { Component, OnInit } from '@angular/core';
import { Product } from '../product';
import { ProductsService } from '../products.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  providers: [ProductsService],
})
export class HomeComponent implements OnInit {
  products: Product[];
  errorMessage: '';
  title = 'Top 3 Products';
  constructor(private productsService: ProductsService) { }

  ngOnInit() {
    this.getProducts();
  }
  getProducts(): void {
    this.productsService.getProducts().subscribe(
                       products => this.products = products.slice(0, 3),
                       error =>  this.errorMessage = <any>error);
  }

}
