import { Pipe, PipeTransform } from '@angular/core';
/*
 * Filter products by filter value
 *
 * Usage:
 *   value | exponentialStrength:exponent
 * Example:
 *   {{ 2 |  exponentialStrength:10}}
 *   formats to: 1024
*/
@Pipe({name: 'productsFilter', pure: false})
export class ProductsFilterPipe implements PipeTransform {
  transform(items: any[], field: string, value: string): any[] {
        if (!items) {
          return [];
        }

        if (value === '' || value === undefined) {
          return items;
        }

        return items.filter(it => it[field] === value);
    }
}
