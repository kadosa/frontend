// https://github.com/nicolas2bert/angular2-orderby-pipe/blob/master/app/orderby.ts
// The pipe class implements the PipeTransform interface's transform method that
// accepts an input value and an optional array of parameters and returns the transformed value.
import { Pipe, PipeTransform } from '@angular/core';
// We tell Angular that this is a pipe by applying the @Pipe decorator which we
// import from the core Angular library.
@Pipe({
  // The @Pipe decorator takes an object with a name property whose value is
  // the pipe name that we'll use within a template expression. It must be a
  // valid JavaScript identifier. Our pipe's name is orderby.
  name: 'orderby'
})
export class OrderByPipe implements PipeTransform {
  transform(array: Array<any>, args?) {
    if (array) {
      if (!args) {
        return array;
      }
      let byVal = 1;
      let orderByValue = '';
      // check if exclamation point
      if (args.charAt(0) === '!') {
        // reverse the array
        byVal = -1;
        orderByValue = args.split('!')[1];
      } else {
        orderByValue = args;
      }

      array.sort((a: any, b: any) => {
        if (a[orderByValue] < b[orderByValue]) {
          return -1 * byVal;
        } else if (a[orderByValue] > b[orderByValue]) {
          return 1 * byVal;
        } else {
          return 0;
        }
      });
      return array;
    }
    //
  }
}
