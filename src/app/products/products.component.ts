import { Component, OnInit } from '@angular/core';
import { Product } from '../product';
import { ProductsService } from '../products.service';
import {OrderByPipe} from '../orderby.pipe';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css'],
  providers: [ProductsService],
})
export class ProductsComponent implements OnInit {
  products: Product[];

  filterValue: '';
  orderBy: '';
  errorMessage: '';

  title = 'All products';
  constructor(private productsService: ProductsService) { }

  ngOnInit() {
    this.getProducts();
  }
  getProducts(): void {
    this.productsService.getProducts().subscribe(
                       products => this.products = products,
                       error =>  this.errorMessage = <any>error);
  }

}
