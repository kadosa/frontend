export class Product {
  name: string;
  rateType: string;
  rate: number;
}
